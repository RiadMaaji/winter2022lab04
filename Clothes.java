public class Clothes {
  private String type;
  private String color;
  private int price;
  
  public Clothes(String type, String color, int price) {
	  this.type = type;
	  this.color = color;
	  this.price = price;
  }
  public void setType(String newType) {
	  this.type = newType;
  }
  public void setColor(String newColor) {
	  this.color = newColor;
  }
  public String getType() {
	  return this.type;
  }
  public String getColor() {
	  return this.color;
  }
  public int getPrice() {
	  return this.price;
  }
  public void colorChoice() {
	  System.out.println("The color of the last product submitted is " + color);
  }
}
