import java.util.Scanner;
public class Shop {
  public static void main(String[] args) {
	  Scanner s = new Scanner(System.in);
	  Clothes[] products = new Clothes[4];
	  for (int i = 0; i < products.length; i++) {
		  System.out.println("Set the type of product #" + (i+1));
		  String type = s.next();
		  System.out.println("Set the color of product #" + (i+1));
		  String color = s.next();
		  System.out.println("Set the price of product #" + (i+1));
		  int price = s.nextInt();
		  products[i] = new Clothes(type, color, price);
	  }
	  System.out.println("Type of the last product: " + products[3].getType());
	  System.out.println("Color of the last product: " + products[3].getColor());
	  System.out.println("Price of the last product: " + products[3].getPrice());
	  System.out.println("Update the type of the last product");
	  products[3].setType(s.next());
	  System.out.println("Update the color of the last product");
	  products[3].setColor(s.next());
	  System.out.println("Type of the last product: " + products[3].getType());
	  System.out.println("Color of the last product: " + products[3].getColor());
	  System.out.println("Price of the last product: " + products[3].getPrice());
  }
}